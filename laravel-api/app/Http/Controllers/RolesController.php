<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    //
     /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $roles = Roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data'    => $roles  
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show(Request $id)
    {
        //find post by ID
        $roles = Roles::findOrfail($id->id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Roles',
            'data'    => $roles 
        ], 200);

    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {   
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Roles::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Data Roles berhasil dibuat',
                'data'    => $roles  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data roles gagal dibuat',
        ], 409);

    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        // $post = Post::findOrFail($id);
        $roles = Roles::find($id);

        if($roles) {

            //update post
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Roles id ' .$roles->id. ' berhasil diupdate',
                'data'    => $roles  
            ], 200);

        }
        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data Roles tidak ditemukan',
        ], 404);
    }

     /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $roles = Roles::find($id);
        // $post = Post::findOrfail($id);

        if($roles) {

            //delete post
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Roles berhasil didelete',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data roles tidak ditemukan',
        ], 404);
    }


}
