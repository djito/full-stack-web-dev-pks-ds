<?php

//class Hewan
trait Hewan {
    //property
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    //method
    public function __construct($name) {
        $this->name = $name;
      }
    abstract public function intro();

    public function atraksi(){
        echo $this->nama." sedang ".$this->keahlian;
    }
}
//Class Fight
trait Fight {
    //property
    public $attackPower;
    public $defencePower;
    private $serang;
    private $diserang;

    //method
    public function serang($serang) {
        echo $this->nama." sedang menyerang ".$serang;
    }
       
    public function diserang($diserang) {
        echo $this->nama." sedang diserang ".$diserang;
        $this->darah = $this->darah - $this->attackPower / $this->defencePower;

    }
}
    


//Class Elang
class Elang {
    use Hewan;
    use Fight;
    $nama = "Elang";
    $jumlahKaki = "2";
    $keahlian = "terbang tinggi";
    $attackPower = 10;
    $defencePower = 5; 

     //method
     public function getInfoHewan() {
         echo $this->nama = "Elang";
         echo $this->jumlahKaki;
         echo $this->keahlian;
         echo $this->attackPower;
         echo $this->defencePower;
     }
}
   

//Class Harimau
class Harimau {
    use Hewan;
    use Fight;
    
        $nama = "Harimau";
        $jumlahKaki = "4";
        $keahlian = "tlari cepat";
        $attackPower = 7;
        $defencePower = 8; 
    
         //method
         public function getInfoHewan() {
             echo $this->nama;
             echo $this->jumlahKaki;
             echo $this->keahlian;
             echo $this->attackPower;
             echo $this->defencePower;
         }
    }




$harimau = new Harimau();
$elang1 = new Elang();

$harimau->serang($elang1);
$elang1->serang($harimau);
?>