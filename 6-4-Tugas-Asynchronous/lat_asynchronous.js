// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini


//=====================latihan

setTimeout(function() {
    console.log("saya dijalankan belakangan")
  }, 3000)
   
  console.log("saya dijalankan pertama") 

  
console.log("ini baris pertama")
console.log("ini baris kedua")
console.log("ini baris ketiga")
/* output dari kode diatas
ini baris pertama
ini baris kedua
ini baris ketiga
*/

setTimeout(() => console.log("ini baris pertama"), 3000)
setTimeout(() => console.log("ini baris kedua"), 1000)
setTimeout(() => console.log("ini baris ketiga"), 2000)
/* output dari kode diatas
ini baris kedua
ini baris ketiga
ini baris pertama
*/
var names = ['surya','adi','jaya','setya'];
console.log(names.entries().next().value);

var names = ['surya','adi','jaya','setya'].entries();
for(var name of names){
 console.log(name);  
}