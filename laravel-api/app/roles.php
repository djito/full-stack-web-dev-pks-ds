<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Str;

class Roles extends Model
{
    //
    protected $fillable =['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    protected static function boot() {
        parent::boot();

        static::creating( function($model){
            if( empty($model->id) ) {
                $model->id = Str::uuid();
            }
        });
    }

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
