// Soal 2 ============================
// Setelah no.1 berhasil, implementasikan function readBooks yang menggunakan callback di atas namun sekarang menggunakan Promise. Buatlah sebuah file dengan nama promise.js. Tulislah sebuah function dengan nama readBooksPromise yang me-return sebuah promise seperti berikut:

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!
// *Petunjuk
// Untuk mengerjakan soal di atas , tidak perlu dilooping, cukup panggil satu satu elemen tiap array nya : books[0] , books[1] , dst.
// Gunakan sisa waktu membaca books[0] untuk membaca books[1], kemudaian sisa waktu membaca books[1] untuk membaca books[2], dst.

// Jawaban Soal 2 ============================
readBooksPromise(10000, books[0])
.then(readBooksPromise(7000, books[1])).catch(error => console.log(error))
.then(readBooksPromise(5000, books[2])).catch(error => console.log(error))
.then(readBooksPromise(1000, books[3])).catch(error => console.log(error))