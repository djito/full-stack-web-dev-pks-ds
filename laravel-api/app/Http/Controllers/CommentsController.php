<?php

namespace App\Http\Controllers;

use App\Comments;
// use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
// use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{   
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    
    //
    /**
     * index
     *
     * @return void
     */
    public function index(Request $request)
    {   
        // dd('masuk sini');

        $post_id = $request->post_id;
        // dd('masuk sini, post id : '.$post_id);
        //get data from table posts
        $comments = Comments::where('post_id', $post_id)->latest()->get();
        // dd('masuk sini, post id : '.$post_id);
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar comments berhasil ditampilkan',
            'data'    => $comments  
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show(Request $id)
    {   
        // dd('masuk sini');
        //find post by ID
        $comments = Comments::findOrfail($id->id);

        if($comments) {
             //make response JSON
            return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $comments 
        ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'data dengan id : '.$id. ' tidak diemukan' ,
            
        ], 404);      

    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {   
        // dd('masuk sini');
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comments::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id,
        ]);

        //kirim email ke Author Post
        // Mail::to($comments->post->user->email)->send(new PostAuthorMail($comments));

        //  //kirim email ke Author/pemilik comment
        //  Mail::to($comments->user->email)->send(new CommentAuthorMail($comments));

        //memanggil event
        event(new CommentStoredEvent($comments));

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Data comments berhasil dibuat',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data comments gagal dibuat',
        ], 409);

    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {   
        
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        // $post = Post::findOrFail($id);
        $comments = Comments::find($id);

        if($comments) {

            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user login',
                    
                ], 403);   
            }

            //update post
            $comments->update([
                'content'     => $request->content,
                
            ]);

            return response()->json([
                'success' => true,
                'message' => 'data dengan coment id : ' .$id. ' Updated',
                'data'    => $comments  
            ], 200);

        }
        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'data dengan coment id : ' .$id. ' tidak ditemukan',
        ], 404);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $comments = Comments::find($id);
        // $post = Post::findOrfail($id);        

        if($comments) {

            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user login',
                    
                ], 403);   
            }

            //delete post
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Comment berhasil dihapus ',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'data dengan coment id : ' .$id. ' tidak ditemukan',
        ], 404);
    }

}