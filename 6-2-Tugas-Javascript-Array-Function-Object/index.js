
 //soal nomor 1 =======================
 console.log("//Jawaban soal nomor 1")
 var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 var urut = 0;
 daftarHewan.sort()
 daftarHewan.forEach(function(item){
    console.log(item)    
 })

 //soal nomor 2 =======================
 //Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

 /* 
    Tulis kode function di sini    
*/
console.log(" ")
console.log("//Jawaban soal nomor 2")

function introduce(item){
   
    return `Nama saya ${item.name}, umur saya ${item.age} tahun, alamat saya di ${item.address}, dan saya punya hobby yaitu ${item.hobby}!`
}

 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//soal 3 =======================
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
console.log(" ")
console.log("//Jawaban soal nomor 3")

function hitung_huruf_vokal(kata){
    var vocal = 0;
    let huruf1 = "";
    for ( let a =0 ; a < kata.length ; a ++){
        huruf1 = kata.charAt(a);
        let huruf = huruf1.toLowerCase() ;
        // console.log(a, huruf)

        if ( huruf != ""){
            if (huruf == "a" || huruf == "i" || huruf == "u" || huruf == "e" || huruf == "o"){
                vocal++;
                // console.log(huruf, vocal)
                }
            }
    };

    return vocal
}

// hitung_huruf_vokal("Muhammad")
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// soal 4 =======================

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.
console.log(" ")
console.log("//Jawaban soal nomor 4")

function hitung(angka) {
    return angka*2-2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
console.log( hitung(6) )