<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * route resource post
 */
// Route::apiResource('/post', 'PostController');

// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');
// Route::get('/post/{id}', 'PostController@show');
// Route::put('/post/{id}', 'PostController@update');
// Route::delete('/post/{id}', 'PostController@destroy');

// Route::get('/comment', 'CommentsController@index');
// Route::post('/comment', 'CommentsController@store');
// Route::get('/comment/{id}', 'CommentsController@show');
// Route::put('/comment/{id}', 'CommentsController@update');
// Route::delete('/comment/{id}', 'CommentsController@destroy');

// Route::get('/roles', 'RolesController@index');
// Route::post('/roles', 'RolesController@store');
// Route::get('/roles/{id}', 'RolesController@show');
// Route::put('/roles/{id}', 'RolesController@update');
// Route::delete('/roles/{id}', 'RolesController@destroy');

Route::apiResource('/post' , 'PostController');
Route::apiResource('/comment' , 'CommentsController');
Route::apiResource('/roles' , 'RolesController');

// Route::post('auth/register', 'Auth\RegisterController');

Route::group([
    'prefix'=> 'auth',
    'namespace'=> 'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationCodeController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login-password', 'LoginController')->name('auth.login_password');
});

