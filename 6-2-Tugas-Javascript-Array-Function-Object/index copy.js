var sayHello = "Hello World!" 
console.log(sayHello)

//
console.log("//.lenght")
var hobbies = ["coding", "cycling", "climbing", "skateboarding"] 
console.log(hobbies) // [ 'coding', 'cycling', 'climbing', 'skateboarding' ]
console.log(hobbies.length) // 4 
 
console.log(hobbies[0]) // coding
console.log(hobbies[2]) // climbing
// Mengakses elemen terakhir dari array
console.log(hobbies[hobbies.length -1]) // skateboarding

//Array
console.log(" ")
console.log("//.Array")
var feeling = ["dag", "dig"]
feeling.push("dug") // menambahkan nilai "dug" ke index paling belakang
console.log(feeling)
feeling.pop() // menghapus nilai pada elemen terakhir array
console.log(feeling)

//.push()
console.log(" ")
console.log("//.push()")
var numbers = [0, 1, 2]
numbers.push(3)
console.log(numbers) // [0, 1, 2, 3]
// Bisa juga memasukkan lebih dari satu nilai menggunakan metode push
numbers.push(4, 5)
console.log(numbers) // [0, 1, 2, 3, 4, 5] 

//.pop()
console.log(" ")
console.log("//.pop()")
var numbers = [0, 1, 2, 3, 4, 5]
numbers.pop() 
console.log(numbers) // [0, 1, 2, 3, 4] 

//.unshift()
console.log(" ")
console.log("//.unshift()")
var numbers = [0, 1, 2, 3]
numbers.unshift(-1) 
console.log(numbers) // [-1, 0, 1, 2, 3]

//.shift()
console.log(" ")
console.log("//.shift()")
var numbers = [ 0, 1, 2, 3]
numbers.shift()
console.log(numbers) // [1, 2, 3] 

//.sort()
console.log(" ")
console.log("//.sort()")
var animals = ["kera", "gajah", "musang"] 
animals.sort()
console.log(animals) // ["gajah", "kera", "musang"]

//.slice()
console.log(" ")
console.log("//.slice()")
var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1,3) 
console.log(irisan1) //[1, 2]
var irisan2 = angka.slice(0,2)
console.log(irisan2) //[0, 1] 

//Jika parameter kedua tidak diisi maka secara otomatis slice akan mengiris array dari indeks di paramer pertama sampai ke indeks terakhir array tersebut.

var angka = [0, 1, 2, 3]
var irisan3 = angka.slice(2)
console.log(irisan3) // [2, 3] 

var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()
console.log(salinAngka) // [0, 1, 2, 3]

//array.splice([IndexMulai], [JumlahNilaiYangDihapus], [NilaiYangDitambahkan1], [NilaiYangDitambahkan2], ...);
//.splice()
console.log(" ")
console.log("//.splice()")
var fruits = [ "banana", "orange", "grape"]
fruits.splice(1, 0, "watermelon") 
console.log(fruits) // [ "banana", "watermelon", "orange", "grape"]

console.log(" ")
var fruits = [ "banana", "orange", "grape"]
fruits.splice(0, 2)
console.log(fruits) // ["grape"]

//.split() dan .join()
//Metode split yaitu memecah sebuah string sehingga menjadi sebuah array. Split menerima sebuah parameter berupa karakter yang menjadi separator untuk memecah string.
console.log(" ")
console.log("//.split()")
var biodata = "name:john,doe" 
var nama = biodata.split(",")
//console.log(name) // [ "name", "john,doe"] 
console.log(nama)

console.log(" ")
console.log("//.join()")
var title = ["my", "first", "experience", "as", "programmer"] 
var slug = title.join("-")
console.log(slug) // "my-first-experience-as-programmer"

//.foreach()
console.log(" ")
console.log("//.foreach()")
var mobil = [{merk: "BMW", warna: "merah", tipe: "sedan"}, {merk: "toyota", warna: "hitam", tipe: "box"}, {merk: "audi", warna: "biru", tipe: "sedan"}]
mobil.forEach(function(item){
    console.log("warna : " + item.warna)
 })


 
var mobil = [{merk: "BMW", warna: "merah", tipe: "sedan"}, {merk: "toyota", warna: "hitam", tipe: "box"}, {merk: "audi", warna: "biru", tipe: "sedan"}]

var arrayWarna = mobil.map(function(item){
    return {merk : item.merk, warna : item.warna, tipe : "sedan"}
 })
 
 console.log(arrayWarna)

mobil.forEach(function(item){
     item.tipe = "jeep"
 })
 
 var car2 = mobil
 console.log(car2)



 //soal nomor 1
 var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 var urut = 0;
 daftarHewan.sort()
 daftarHewan.forEach(function(item){
    console.log(item)    
 })

 //soal nomor 2
 //Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

 /* 
    Tulis kode function di sini    
*/
function introduce(item){
   
    return `Nama saya ${item.name}, umur saya ${item.age} tahun, alamat saya di ${item.address}, dan saya punya hobby yaitu ${item.hobby}!`
}

 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

function hitung_huruf_vokal(kata){
    var vocal = 0;
    let huruf1 = "";
    for ( let a =0 ; a < kata.length ; a ++){
        huruf1 = kata.charAt(a);
        let huruf = huruf1.toLowerCase() ;
        // console.log(a, huruf)

        if ( huruf != ""){
            if (huruf == "a" || huruf == "i" || huruf == "u" || huruf == "e" || huruf == "o"){
                vocal++;
                // console.log(huruf, vocal)
                }
            }
    };

    return vocal
}

// hitung_huruf_vokal("Muhammad")
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// soal 4

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

function hitung(angka) {
    return angka*2-2
}


console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
console.log( hitung(6) )