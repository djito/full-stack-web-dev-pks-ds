// Soal 1 ========================
// Kita mempunyai tumpukan buku untuk dibaca. Setiap buku memiliki waktu yang dibutuhkan untuk menghabiskan buku tersebut. Sudah disediakan function readBooks yang menerima tiga parameter: waktu, buku yang dibaca, dan sebuah callback. Salin code berikut ke dalam sebuah file bernama callback.js .
var readBooks = require('./callback.js')
 
var books = [
    {nama: 'LOTR', timeSpent: 3000}, 
    {nama: 'Fidas', timeSpent: 2000}, 
    {nama: 'Kalkulus', timeSpent: 4000},
    {nama: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
// lanjutkan code pada index.js untuk memanggil function readBooks. Buku yang akan dihabiskan adalah buku-buku di dalam array books. Pertama function readBooks menerima input waktu yang dimiliki yaitu 10000 ms (10 detik) dan books pada indeks ke-0. Setelah mendapatkan callback sisa waktu yang dikirim lewat callback, sisa waktu tersebut dipakai untuk membaca buku pada indeks ke-1. Begitu seterusnya sampai waktu habis atau semua buku sudah terbaca.

// Jawaban Soal 1 ========================
function callback(time) {
    return time    
}

readBooks(10000, books[0], callback)
readBooks(7000, books[1], callback)
readBooks(5000, books[2], callback)
readBooks(1000, books[3], callback)