<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {   
        // dd('masuk disini');
        //
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [            
            'otp' => 'required',            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp',  $request->otp)->first();

        if(!$otp_code)
        {
            return response()->json([
                'success' => false,
                'message' => 'Otp Code tidak ditemukan',
            ], 400);
        }

        $now = Carbon::now();

        if ($now > $otp_code->valid_until)
        {
            return response()->json([
                'success' => false,
                'message' => 'Otp Code tidak berlaku lagi',
            ], 400);
        }

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'Otp Code berhasil diverifikasi',
            'data' => $user
        ]);




    }
}
