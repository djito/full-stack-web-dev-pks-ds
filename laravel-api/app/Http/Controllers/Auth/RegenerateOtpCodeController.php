<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [            
            'email' => 'required',            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        //email yang dibuat sebelumnya
        // dd($user->otp_code);

        if($user->otp_code) {
            $user->otp_code->delete();
        }
        
        // dd($user->otp_code);


        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp'=> $random,
            'valid_until'=> $now->addMinute(5),
            'user_id' => $user->id,
            
        ]);

        //kirim email otp code ke email register

        return response()->json([
            'success' => true,
            'message' => 'Otp Code berhasil digenerate',
            'data'    => [
                'user' => $user,
                'otp_code' => $otp_code
            ]  
        ], 200);
        

    }
}
