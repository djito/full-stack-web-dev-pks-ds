<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Str;

class Comments extends Model
{
    //
    protected $fillable =['content', 'post_id', 'user_id'  ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    protected static function boot() {
        parent::boot();

        static::creating( function($model){
            if( empty($model->id) ) {
                $model->id = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}